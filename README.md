# role-cisco-interfaces

Ansible Playbook zum Konfigurieren der Cisco Interfaces


Use Variables for configuration e.g.:
```
interface:
  default:
    name: "Default Interface"
    vlan: 0
  GigabitEthernet0/7:
    name: "Freifunk Input"
    vlan: 7
```
vlan 0 -> no vlan
vlan t -> Trunk
